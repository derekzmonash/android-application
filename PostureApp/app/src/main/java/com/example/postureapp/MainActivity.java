package com.example.postureapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;

import android.text.format.DateFormat;
import android.util.Log;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class MainActivity extends Activity implements BluetoothAdapter.LeScanCallback {
    private static final String TAG = "BluetoothGattActivity";

    private static final String DEVICE_NAME = "PosturePrototype";

    private static final UUID CONFIG_DESCRIPTOR = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    // Defining UUIDs of different services and characteristics
    public static final UUID BATTERYSERVICE = UUID.fromString("000018a1-1100-1000-8000-00805f9b34fb");
    public static final UUID BATTERYLEVEL = UUID.fromString("00008ab1-1100-1000-8000-00805f9b34fb");

    public static final UUID ESKINSERVICE = UUID.fromString("000018a2-1100-1000-8000-00805f9b34fb");
    public static final UUID ESKINVAL = UUID.fromString("00008ab3-1100-1000-8000-00805f9b34fb");

    public static final UUID ACCSERVICE = UUID.fromString("000018a4-1100-1000-8000-00805f9b34fb");
    public static final UUID ACCVAL = UUID.fromString("00008ab4-1100-1000-8000-00805f9b34fb");

    public static final UUID POSTURESERVICE = UUID.fromString("000018a8-1100-1000-8000-00805f9b34fb");
    public static final UUID POSTUREVAL = UUID.fromString("00008ac0-1100-1000-8000-00805f9b34fb");

    public static final UUID GYROSERVICE = UUID.fromString("000018a7-1100-1000-8000-00805f9b34fb");
    public static final UUID GYROXVAL = UUID.fromString("00008ab7-1100-1000-8000-00805f9b34fb");
    public static final UUID GYROYVAL = UUID.fromString("00008ab8-1100-1000-8000-00805f9b34fb");
    public static final UUID GYROZVAL = UUID.fromString("00008ab9-1100-1000-8000-00805f9b34fb");

    public static final UUID TIMESERVICE = UUID.fromString("000018a9-1100-1000-8000-00805f9b34fb");
    public static final UUID TIMEVAL = UUID.fromString("00008ac1-1100-1000-8000-00805f9b34fb");


    private BluetoothManager manager;
    private BluetoothAdapter mBluetoothAdapter;
    private SparseArray<BluetoothDevice> mDevices;

    private BluetoothGatt mConnectedGatt;

    private TextView meskinl,meskinr,mgyrox,mgyroz,mgyroy,mposture,mbattstat,mbattvolt,mconnectionstat,mtimer,maccx,maccz,maccy,mclocktime;
    int hour, minute, second;
    private ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.activity_main);
        setProgressBarIndeterminate(true);


        /*
         * Display results in defined text fields
         */
        meskinl = (TextView) findViewById(R.id.eskinldata);
        meskinr = (TextView) findViewById(R.id.eskinrdata);
        mgyrox = (TextView) findViewById(R.id.gyroxdata);
        mgyroy = (TextView) findViewById(R.id.gyroydata);
        mgyroz = (TextView) findViewById(R.id.gyrozdata);
        maccx = (TextView) findViewById(R.id.accxdata);
        maccy = (TextView) findViewById(R.id.accydata);
        maccz = (TextView) findViewById(R.id.acczdata);
        mposture = (TextView) findViewById(R.id.posturestatus);
        mbattstat = (TextView) findViewById(R.id.battstatus);
        mbattvolt = (TextView) findViewById(R.id.battvolt);
        mclocktime = (TextView) findViewById(R.id.clocktime);
        mconnectionstat = (TextView) findViewById(R.id.Connection_status);
        mtimer = (TextView) findViewById(R.id.timerdata);

        // Creating the bluetooth manager object
        manager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        mBluetoothAdapter = manager.getAdapter();

        mDevices = new SparseArray<BluetoothDevice>();

        /*
         * Creating a progress dialogue to inform the user
         */
        mProgress = new ProgressDialog(this);
        mProgress.setIndeterminate(true);
        mProgress.setCancelable(false);



    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
         * Check if device has bluetooth turned on
         */
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            //Bluetooth is disabled
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivity(enableBtIntent);
            finish();
            return;
        }

        /*
         * Check for Bluetooth LE Support
         */
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "No LE Support.", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        clearDisplayValues();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Make sure dialog is hidden
        mProgress.dismiss();
        //Cancel any scans in progress
        mHandler.removeCallbacks(mStopRunnable);
        mHandler.removeCallbacks(mStartRunnable);
        mBluetoothAdapter.stopLeScan(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Disconnect from any active tag connection
        if (mConnectedGatt != null) {
            mConnectedGatt.disconnect();
            mConnectedGatt = null;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Add the "scan" option to the menu
        getMenuInflater().inflate(R.menu.main, menu);
        Log.i(TAG, "Menu Hit");
        //Add any device elements we've discovered to the overflow menu
        for (int i=0; i < mDevices.size(); i++) {
            BluetoothDevice device = mDevices.valueAt(i);
            menu.add(0, mDevices.keyAt(i), 0, device.getName());
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_scan:
                mDevices.clear();
                startScan();
                return true;
            case R.id.disconnect:
                mConnectedGatt.disconnect();
                return true;
            default:
                //Obtain the discovered device to connect with
                BluetoothDevice device = mDevices.get(item.getItemId());
                Log.i(TAG, "Connecting to "+device.getName());
                /*
                 * Connecting to desired device
                 */
                mConnectedGatt = device.connectGatt(this, false, mGattCallback);
                //Display progress UI
                mHandler.sendMessage(Message.obtain(null, MSG_PROGRESS, "Connecting to "+device.getName()+"..."));
                return super.onOptionsItemSelected(item);
        }
    }

    private void clearDisplayValues() {
        //Resets displays
        mgyrox.setText("---");
        mgyroy.setText("---");
        mgyroz.setText("---");
        maccx.setText("---");
        maccy.setText("---");
        maccz.setText("---");
        mtimer.setText("---");
        meskinl.setText("---");
        meskinr.setText("---");
        mposture.setText("---");
        mbattstat.setText("---");
        mbattvolt.setText("---");
        mclocktime.setText("--:--:--");
        mconnectionstat.setText("Disconnected");
    }


    private Runnable mStopRunnable = new Runnable() {
        @Override
        public void run() {
            stopScan();
        }
    };
    private Runnable mStartRunnable = new Runnable() {
        @Override
        public void run() {
            startScan();
        }
    };

    private void startScan() {
        mBluetoothAdapter.startLeScan(this);
        setProgressBarIndeterminateVisibility(true);
        Log.i(TAG,"Scan Started");
        mHandler.postDelayed(mStopRunnable, 3000);
    }

    private void stopScan() {
        mBluetoothAdapter.stopLeScan(this);
        setProgressBarIndeterminateVisibility(false);
    }

    /* BluetoothAdapter.LeScanCallback */

    @Override
    public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
        Log.i(TAG, "New LE Device: " + device.getName() + " @ " + rssi);
        /*
         * Filtering out irrelevant devices so that we only get one device on the list
         */
        if (DEVICE_NAME.equals(device.getName())) {
            mDevices.put(device.hashCode(), device);
            //Update the overflow menu
            invalidateOptionsMenu();
        }
    }

    /*
     * Creating a state machine to set and read all outputs
     */
    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        /* State Machine Tracking */
        private int mState = 0;

        private void reset() { mState = 0; }

        private void advance() {
            if(false){  //If statement for debugging only
                mState=9;
            }
            else{
                mState++;
            }
            }

        /*
         * Send an enable command to each sensor. NOTE: This section is unused
         */
        private void enableNextSensor(BluetoothGatt gatt) {
            BluetoothGattCharacteristic characteristic;
            switch (mState) {

                default:
                    mHandler.sendEmptyMessage(MSG_DISMISS);
                    Log.i(TAG, "All Sensors Enabled");
                    return;
            }

            //gatt.writeCharacteristic(1);
        }

        /*
         * Read the data characteristic value for each sensor
         */
        private void readNextSensor(BluetoothGatt gatt) {
            BluetoothGattCharacteristic characteristic;
            switch (mState) {
                case 0:
                    Log.d(TAG, "Reading Eskin");
                    characteristic = gatt.getService(ESKINSERVICE)
                            .getCharacteristic(ESKINVAL);
                    break;
                case 1:
                    Log.d(TAG, "Reading Battery");
                    characteristic = gatt.getService(BATTERYSERVICE)
                            .getCharacteristic(BATTERYLEVEL);
                    break;

                case 2:
                    Log.d(TAG, "Reading GyroX");
                    characteristic = gatt.getService(GYROSERVICE)
                            .getCharacteristic(GYROXVAL);
                    break;

                case 3:
                    Log.d(TAG, "Reading GyroY");
                    characteristic = gatt.getService(GYROSERVICE)
                            .getCharacteristic(GYROYVAL);
                    break;

                case 4:
                    Log.d(TAG, "Reading GyroZ");
                    characteristic = gatt.getService(GYROSERVICE)
                            .getCharacteristic(GYROZVAL);
                    break;

                case 5:
                    Log.d(TAG, "Reading Acc");
                    characteristic = gatt.getService(ACCSERVICE)
                            .getCharacteristic(ACCVAL);
                    Log.w(TAG,"Updating acc Value" + characteristic);
                    break;

                case 6:
                    Log.d(TAG, "Reading Posture");
                    characteristic = gatt.getService(POSTURESERVICE)
                            .getCharacteristic(POSTUREVAL);
                    break;
                case 7:
                    Log.d(TAG, "Reading Time");
                    characteristic = gatt.getService(TIMESERVICE)
                            .getCharacteristic(TIMEVAL);
                    break;
                default:
                    mHandler.sendEmptyMessage(MSG_DISMISS);
                    Log.i(TAG, "All Sensors Enabled");
                    return;
            }

            gatt.readCharacteristic(characteristic);
        }

        /*
         * Enable notification of changes on the data characteristic for each sensor
         */
        private void setNotifyNextSensor(BluetoothGatt gatt) {
            BluetoothGattCharacteristic characteristic;
            switch (mState) {
                case 0:
                    Log.d(TAG, "Reading Eskin");
                    characteristic = gatt.getService(ESKINSERVICE)
                            .getCharacteristic(ESKINVAL);
                    break;
                case 1:
                    Log.d(TAG, "Reading Battery");
                    characteristic = gatt.getService(BATTERYSERVICE)
                            .getCharacteristic(BATTERYLEVEL);
                    break;

                case 2:
                    Log.d(TAG, "Reading GyroX");
                    characteristic = gatt.getService(GYROSERVICE)
                            .getCharacteristic(GYROXVAL);
                    break;

                case 3:
                    Log.d(TAG, "Reading GyroY");
                    characteristic = gatt.getService(GYROSERVICE)
                            .getCharacteristic(GYROYVAL);
                    break;

                case 4:
                    Log.d(TAG, "Reading GyroZ");
                    characteristic = gatt.getService(GYROSERVICE)
                            .getCharacteristic(GYROZVAL);
                    break;

                case 5:
                    Log.d(TAG, "Reading Acc");
                    characteristic = gatt.getService(ACCSERVICE)
                            .getCharacteristic(ACCVAL);
                    break;

                case 6:
                    Log.d(TAG, "Reading Posture");
                    characteristic = gatt.getService(POSTURESERVICE)
                            .getCharacteristic(POSTUREVAL);
                    break;
                case 7:
                    Log.d(TAG, "Reading Time");
                    characteristic = gatt.getService(TIMESERVICE)
                            .getCharacteristic(TIMEVAL);
                    break;
                default:
                    mHandler.sendEmptyMessage(MSG_DISMISS);
                    Log.i(TAG, "All Sensors Enabled");
                    return;
            }

            //Enable local notifications
            gatt.setCharacteristicNotification(characteristic, true);
            //Enabled remote notifications
            BluetoothGattDescriptor desc = characteristic.getDescriptor(CONFIG_DESCRIPTOR);
            desc.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(desc);
        }

        @Override
        // States changes in the application depending on connection status
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(TAG, "Connection State Change: "+status+" -> "+connectionState(newState));
            if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_CONNECTED) {

                gatt.discoverServices();
                mconnectionstat.setText("Connected");
                mHandler.sendMessage(Message.obtain(null, MSG_PROGRESS, "Discovering Services..."));

            } else if (status == BluetoothGatt.GATT_SUCCESS && newState == BluetoothProfile.STATE_DISCONNECTED) {

                mHandler.sendEmptyMessage(MSG_CLEAR);
                mconnectionstat.setText("Disconnected");
            } else if (status != BluetoothGatt.GATT_SUCCESS) {
                /*
                 * If there is a failure at any stage, simply disconnect
                 */
                gatt.disconnect();
                mconnectionstat.setText("Disconnected");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Log.d(TAG, "Services Discovered: "+status);
            mHandler.sendMessage(Message.obtain(null, MSG_PROGRESS, "Enabling Sensors..."));
            /*
             * With services discovered, we are going to reset our state machine and start
             * reading each value
             */
            reset();
            readNextSensor(gatt);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            //For each read, pass the data up to the UI thread to update the display

            if (ESKINVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGESKIN, characteristic));
            }
            if (BATTERYLEVEL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGBATTERY, characteristic));
            }
            if (GYROXVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGGYROX, characteristic));
            }

            if (GYROYVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGGYROY, characteristic));
            }

            if (GYROZVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGGYROZ, characteristic));
            }
            if (ACCVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGACC, characteristic));
            }
            if (POSTUREVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGPOSTURE, characteristic));
            }
            if (TIMEVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGTIME, characteristic));
            }


            //After reading the initial value, enable notifications next

            setNotifyNextSensor(gatt);
        }

        // When a characteristic is written it will send a response and the action is defined here. NOTE: This section is also unused
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            //After writing the enable flag, next we read the initial value
            readNextSensor(gatt);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            /*
             * After enabling notifications, all changes in the data will be handled here
             */
            if (ESKINVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGESKIN, characteristic));
            }
            if (BATTERYLEVEL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGBATTERY, characteristic));
            }
            if (GYROXVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGGYROX, characteristic));
            }

            if (GYROYVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGGYROY, characteristic));
            }

            if (GYROZVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGGYROZ, characteristic));
            }
            if (ACCVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGACC, characteristic));
            }
            if (POSTUREVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGPOSTURE, characteristic));
            }
            if (TIMEVAL.equals(characteristic.getUuid())) {
                mHandler.sendMessage(Message.obtain(null, MSGTIME, characteristic));
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            //Once notifications are enabled, next sensor is read
            advance();
            readNextSensor(gatt);
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            Log.d(TAG, "Remote RSSI: "+rssi);
        }

        private String connectionState(int status) {
            switch (status) {
                case BluetoothProfile.STATE_CONNECTED:
                    return "Connected";
                case BluetoothProfile.STATE_DISCONNECTED:
                    return "Disconnected";
                case BluetoothProfile.STATE_CONNECTING:
                    return "Connecting";
                case BluetoothProfile.STATE_DISCONNECTING:
                    return "Disconnecting";
                default:
                    return String.valueOf(status);
            }
        }
    };

    /*
     * We have a Handler to process event results on the main thread
     */
    private static final int MSGBATTERY = 101;
    private static final int MSGPOSTURE = 201;
    private static final int MSGESKIN = 301;
    private static final int MSGGYROX = 401;
    private static final int MSGGYROY = 402;
    private static final int MSGGYROZ = 403;
    private static final int MSGACC = 501;
    private static final int MSG_PROGRESS = 601;
    private static final int MSG_DISMISS = 602;
    private static final int MSG_CLEAR = 701;
    private static final int MSGTIME = 801;

    // Depending on the message sent, the handler will decide what characteristic is changed and take appropriate action
    private Handler mHandler = new Handler() {
        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(Message msg) {
            BluetoothGattCharacteristic characteristic;
            switch (msg.what) {
                case MSGBATTERY:
                    characteristic = (BluetoothGattCharacteristic) msg.obj;
                    if (characteristic.getValue() == null) {
                        Log.w(TAG, "Error obtaining battery value");
                        return;
                    }

                    updatebattery(characteristic);
                    break;
                case MSGPOSTURE:
                    characteristic = (BluetoothGattCharacteristic) msg.obj;
                    if (characteristic.getValue() == null) {
                        Log.w(TAG, "Error obtaining posture value");
                        return;
                    }
                    updateposture(characteristic);
                    break;
                case MSGESKIN:
                    characteristic = (BluetoothGattCharacteristic) msg.obj;
                    if (characteristic.getValue() == null) {
                        Log.w(TAG, "Error obtaining eskin value");
                        return;
                    }
                    //Log.w(TAG,"R Update");
                    updateeskin(characteristic);
                    break;
                case MSGGYROX:
                    characteristic = (BluetoothGattCharacteristic) msg.obj;
                    if (characteristic.getValue() == null) {
                        Log.w(TAG, "Error obtaining gyrox value");
                        return;
                    }

                    updategyrox(characteristic);
                    break;
                case MSGGYROY:
                    characteristic = (BluetoothGattCharacteristic) msg.obj;
                    if (characteristic.getValue() == null) {
                        Log.w(TAG, "Error obtaining gyroy value");
                        return;
                    }
                    updategyroy(characteristic);
                    break;
                case MSGGYROZ:
                    characteristic = (BluetoothGattCharacteristic) msg.obj;
                    if (characteristic.getValue() == null) {
                        Log.w(TAG, "Error obtaining gyroz value");
                        return;
                    }

                    updategyroz(characteristic);
                    break;
                case MSGACC:
                    characteristic = (BluetoothGattCharacteristic) msg.obj;
                    if (characteristic.getValue() == null) {
                        Log.w(TAG, "Error obtaining acc value");
                        return;
                    }
                    //Log.w(TAG,"Updating acc Value" + characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16,0));
                    updateacc(characteristic);
                    break;
                case MSGTIME:
                    characteristic = (BluetoothGattCharacteristic) msg.obj;
                    if (characteristic.getValue() == null) {
                        Log.w(TAG, "Error obtaining time value");
                        return;
                    }

                    updatetimer(characteristic);
                    break;
                case MSG_PROGRESS:
                    mProgress.setMessage((String) msg.obj);
                    if (!mProgress.isShowing()) {
                        mProgress.show();
                    }
                    break;
                case MSG_DISMISS:
                    mProgress.hide();
                    break;
                case MSG_CLEAR:
                    clearDisplayValues();
                    break;
            }
        }
    };

    // Functions here will take the value from the data and print them to the text output
    private void updatebattery(BluetoothGattCharacteristic characteristic){
        double battvoltread = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16,0);
        //Log.w(TAG,"Updating Voltage" + battvoltread);
        double battvoltcalc = battvoltread/100;
        mbattvolt.setText(String.valueOf(battvoltcalc)+"V");
        Log.w(TAG,"Updating Voltage" + battvoltread);
        if(battvoltcalc <= 4.55){
            mbattstat.setText("LOW");
        }
        else{
            mbattstat.setText("OK");
        }
    }

    private void updateeskin(BluetoothGattCharacteristic characteristic){

        //meskinr.setText(String.valueOf(characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16,0)));
        long eskinbt = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32,0);
        long eskinL = eskinbt/10000;
        long eskinR = eskinbt%10000;

        meskinl.setText(String.valueOf(eskinL));
        meskinr.setText(String.valueOf(eskinR));
    }
    // On change of posture it will check if the posture is bad, and if so, trigger notifications
    private void updateposture(BluetoothGattCharacteristic characteristic){
        //Log.w(TAG,"Updating Posture Value" + characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8,0));
        int poststat = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16,0);
        if (poststat == 1){
            mposture.setText("Good");

        }
        if (poststat == 2){
            mposture.setText("Bad");
            //Toast toast = Toast.makeText(this,"Warning: Bad Posture", Toast.LENGTH_SHORT);
            //toast.show();

            NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();


            Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent,0);

            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            String NOTIFICATION_CHANNEL_ID = "101";

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_MAX);

                //Configure Notification Channel
                notificationChannel.setDescription("Posture Notification");
                notificationChannel.enableLights(true);
                notificationChannel.setVibrationPattern(new long[]{50,10});
                notificationChannel.enableVibration(true);

                notificationManager.createNotificationChannel(notificationChannel);
            }

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setContentTitle("Posture Companion")
                    .setAutoCancel(true)
                    .setSound(defaultSound)
                    .setContentText("Bad Posture Detected")
                    .setContentIntent(pendingIntent)
                    .setStyle(style)
                    .setWhen(System.currentTimeMillis())
                    .setPriority(Notification.PRIORITY_MAX);


            notificationManager.notify(1, notificationBuilder.build());
        }
        if(poststat == 0){
            mposture.setText("???");
        }
    }

    // For gyro values, as they were in float on the arduino end, they must be converted back to a float by dividing by 100, as the data is send as an interger
    private void updategyrox(BluetoothGattCharacteristic characteristic){
        double gyrox100 = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16,0);
        //Log.w(TAG,"Updating gyro Value" + gyrox100);
        double gyrox = gyrox100/100;
        mgyrox.setText(String.valueOf(gyrox));



    }
    private void updategyroy(BluetoothGattCharacteristic characteristic){
        double gyroy100 = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16,0);
        double gyroy=gyroy100/100;
        mgyroy.setText(String.valueOf(gyroy));
    }
    private void updategyroz(BluetoothGattCharacteristic characteristic){
        double gyroz100 = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT16,0);
        double gyroz=gyroz100/100;
        mgyroz.setText(String.valueOf(gyroz));
    }
    private void updateacc(BluetoothGattCharacteristic characteristic){
        long accbt = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_SINT32,0);
        //Log.w(TAG,"Updating acc Value" + accbt);

        long accx = accbt/1000000;
        //Log.w(TAG,"Updating accx Value" + accx);
        if(accx>800){
            accx = (1000-accx)*-1;
        }
        double accxout= accx;
        accxout=accxout/100;

        long tempaccbt = accbt%1000000;
        long accy = tempaccbt/1000;

        if(accy>800){
            accy = (1000-accy)*-1;
            //Log.w(TAG,"Updating accz Value" + accy);
        }
        double accyout= accy;
        accyout=accyout/100;
        //Log.w(TAG,"Updating accy Value" + accyout);
        long accz = tempaccbt%1000;

        if(accz>800){
            accz = (1000-accz)*-1;

        }
        double acczout= accz;
        acczout=acczout/100;

        maccx.setText(String.valueOf(accxout));
        maccy.setText(String.valueOf(accyout));
        maccz.setText(String.valueOf(acczout));



    }

    int newstat = 1;
    //The timer is set, and is updated whenever all the data is read. This function also write all the data to a csv file.
    private void updatetimer(BluetoothGattCharacteristic characteristic){
        long timestat = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32,0);

        mtimer.setText(String.valueOf(timestat));

        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);
        second = calendar.get(Calendar.SECOND);

        mclocktime.setText(String.valueOf(hour) + ":" +String.valueOf(minute)+":"+String.valueOf(second));



        String csv = this.getExternalFilesDir(null).getAbsolutePath()+ "Posturedata.csv"; // Here csv file name is posturedata.csv
        CSVWriter writer = null;

        try
        {
            List<String[]> data = new ArrayList<String[]>();
            writer = new CSVWriter(new FileWriter(csv, true));
            if (newstat==1){
                writer = new CSVWriter(new FileWriter(csv, false));
                newstat=0;
                data.add(new String[]{"Time (H:M:S)","Time elapsed (Ms)","Eskin L", "EskinR","AccX","AccY","AccZ","GyroX","GyroY","GyroZ","Posture","\n"});

            }



            data.add(new String[]{mclocktime.getText().toString(),mtimer.getText().toString(),meskinl.getText().toString(), meskinr.getText().toString(),maccx.getText().toString(),maccy.getText().toString(),maccz.getText().toString(),mgyrox.getText().toString(),mgyroy.getText().toString(),mgyroz.getText().toString(),mposture.getText().toString(),"\n"});


            writer.writeAll(data); // data is adding to csv

            writer.close();

        }
        catch (IOException e)
        {
            Log.e(TAG,"Error writing");//error
        }
    }
}